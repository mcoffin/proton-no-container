#!/bin/bash
set -e

script_dir="$(realpath "$(dirname "$0")")"

missing_required() {
	printf 'Missing required argument: %s\n' "$1"
}
print_usage() {
	local invocation="${1:-make-overlay.sh}"
	printf 'Usage: %s -s SOURCE_DIR [-n NAME] TARGET_DIR\n' "$invocation"
}

while getopts ':s:n:hdf' opt; do
	case ${opt} in
		s)
			source_dir="$OPTARG"
			;;
		n)
			target_name="$OPTARG"
			;;
		h)
			print_usage "$0"
			exit $?
			;;
		d)
			dry_run=1
			;;
		f)
			force_mode=1
			;;
		\?)
			echo "Unknown argument: -$OPTARG" >&2
			print_usage >&2
			exit 1
			;;
		:)
			printf 'Invalid argument: -%s requires an argument' "$OPTARG" >&2
			print_usage >&2
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

get_source_name() {
	if [ $# -lt 1 -o -z "$1" ]; then
		echo 'Usage: get_source_name PATH' >&2
		return 1
	fi
	awk '{ if (NF < 2) { next; } for (i = 2; i < NF; i++) { printf "%s", $i; } print $NF; }' < "$1"/version | tr -d '\n'
}

ensure_dir() {
	local d
	local ret=0
	set +e
	for d in "$@"; do
		[ -e "$d" ] || mkdir -p "$d"
		local tmpret=$?
		[ $tmpret -eq 0 ] || ret=$tmpret
	done
	set -e
	return $tmpret
}

dump_values() {
	while [ $# -ge 2 ]; do
		printf '\033[1;36m%s\033[0m\033[2m=\033[0m%s\n' "$1" "$2"
		shift 2
	done
}

findn() {
	local n="$1"
	local path="$2"
	shift 2
	find "$path" -mindepth "$n" -maxdepth "$n" "$@"
}

find1() {
	findn 1 "$@"
}

dir_empty() {
	local d
	for d in "$@"; do
		[ -e "$d" ] || continue
		if [ $(find1 "$d" | wc -l) -gt 0 ]; then
			return 1
		fi
	done
}

if [ $# -lt 1 -o -z "$1" ]; then
	missing_required TARGET_DIR >&2
	print_usage >&2
	exit 1
fi
if [ -z "$source_dir" ] || [ ! -e "$source_dir" ]; then
	msising_required SOURCE_DIR >&2
	print_usage >&2
	exit 1
fi
[ ! -z "$target_name" ] || target_name="$(basename "$target_dir")"
target_dir="$1"
source_name="$(get_source_name "$source_dir")"
if [ -z "$source_name" ]; then
	printf 'Failed to get source name at path: %s\n' "$source_dir" >&2
	exit 1
fi

dump_values \
	source_dir "$source_dir" \
	source_name "$source_name" \
	target_dir "$target_dir" \
	target_name "$target_name"

if [ ${dry_run:-0} -ne 0 ]; then
	echo 'Stopping because of dry-run flag' >&2
	exit 0
fi

if ! dir_empty "$target_dir"; then
	if [ ${force_mode:-0} -eq 0 ]; then
		printf 'Target path \033[1;36m%s\033[0m is not empty. Refusing to continue.\n' "$target_dir" >&2
		echo 'Use -f to force' >&2
		exit 1
	else
		printf 'Clearing out existing target path: %s\n' "$target_dir"
		rm -rf "$target_dir"
	fi
fi
ensure_dir "$target_dir"
pushd "$target_dir" 1> /dev/null
awk -v target_name="$target_name" '{ if (NF > 0) { printf "%d %s\n", $1, target_name; } }' < "$source_dir"/version > version
grep -vF 'require_tool_appid' "$source_dir"/toolmanifest.vdf > toolmanifest.vdf
m4 -D __NAME__="$target_name" "$script_dir/proto/compatibilitytool.vdf" > compatibilitytool.vdf
echo -n "$source_dir" > base.txt
popd 1> /dev/null

_diff() {
	diff --color=always "$@" || true
}
for f in $(find1 "$target_dir" -type f -print0 | xargs -0 -n 1 basename); do
	source_f="$source_dir"/"$f"
	[ -e "$source_f" ] || source_f=/dev/null
	printf '\033[1;36m%s\033[0m\033[2m:\033[0m\n' "$f"
	_diff "$source_f" "$target_dir/$f" || true
done
