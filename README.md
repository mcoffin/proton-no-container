# Usage

```bash
./make-overlay.sh -f -s ~/.local/share/Steam/steamapps/common/Proton\ 5.13 -n NProton-5.13-6 /opt/faststeam/NProton-5.13-6.bak
./mount-overlay.sh -S /opt/faststeam/NProton-5.13-6.bak
```
