#!/bin/bash
set -e

script_dir="$(realpath "$(dirname "$0")")"

_sudo() {
	local prefix=''
	[ "$(whoami)" == "root" ] || prefix='sudo'
	$prefix $@
	return $?
}
print_usage() {
	local invocation="${1:-mount-overlay.sh}"
	printf 'Usage: %s [-s COMPATIBILITY_PATH] TARGET_PATH\n' "$invocation"
}

compatibility_path=~/.local/share/Steam/compatibilitytools.d
mode=mount
while getopts ':s:hS' opt; do
	case ${opt} in
		s)
			compatibility_path="$OPTARG"
			;;
		S)
			mode=systemd
			;;
		h)
			print_usage "$0"
			exit $?
			;;
		\?)
			echo "Unknown argument: -$OPTARG"
			exit 1
			;;
		:)
			printf 'Invalid argument: -%s requires an argument' "$OPTARG" >&2
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

if [ $# -lt 1 ] || [ ! -e "$1" ]; then
	echo 'Missing required argument: TARGET_PATH' >&2
	print_usage "$0" >&2
	exit 1
fi
if [ ! -e "$compatibility_path" ]; then
	printf 'Compatbility path \033[1;36m%s\033[0m does not exist!\n' "$compatibility_path" >&2
	exit 1
fi

source_path="$1"
source_name="$(awk 'BEGIN { done = 0; } { if (NF < 2 || done > 0) { next; } printf "%s", $2; done++; }' < "$source_path"/version)"

if [ -z "$source_name" ]; then
	printf 'Failed to find name at source path: %s\n' "$source_path" >&2
	exit 1
fi

print_values() {
	local color=0
	local opt OPTIND OPTARG
	while getopts ':c' opt; do
		case ${opt} in
			c)
				color=1
				;;
			\?)
				echo "Unknown argument: -$OPTARG" >&2
				return 1
				;;
			:)
				printf 'Invalid argument: -%s requires an argument\n' "$OPTARG" >&2
				return 1
				;;
		esac
	done
	shift $((OPTIND - 1))
	local pattern='\033[1;36m%s\033[0m\033[2m:\033[0m %s\n'
	if [ ! -t 1 ] && [ $color -eq 0 ]; then
		pattern='%s: %s\n'
	fi
	while [ $# -ge 2 ]; do
		printf "$pattern" "$1" "$2"
		shift 2
	done
}

ensure_dir() {
	while [ $# -gt 0 ]; do
		[ -e "$1" ] || mkdir -p "$1"
		shift
	done
}

dir_empty() {
	local d
	for d in "$@"; do
		[ -e "$d" ] || continue
		[ $(find "$d" -mindepth 1 -maxdepth 1 | wc -l) -eq 0 ] || return 1
	done
	return 0
}

target_dir="$compatibility_path"/"$source_name"
base_dir="$(head -n 1 < "$source_path"/base.txt | tr -d '\n')"
print_values \
	source_path "$source_path" \
	source_name "$source_name" \
	target_dir "$target_dir" \
	base_dir "$base_dir" >&2

systemd_escape() {
	systemd-escape -p "$1" | head -n 1 | tr -d '\n'
}
dump_systemd() {
	echo '[Unit]'
	printf 'Description=Proton %s mount point\n' "$source_name"
	local source_mount="$(df "$source_path" | tail -n 1 | awk '{ print $NF; }' | tr -d '\n')"
	if [ "$source_mount" != "/" ]; then
		source_mount="$(systemd_escape "$source_mount").mount"
		print_values source_mount "$source_mount" >&2
		printf 'Requires=%s\nAfter=%s\n' "$source_mount" "$source_mount"
	fi
	printf '\n[Mount]\n'
	echo 'Type=overlay' && echo 'What=overlay'
	printf '%s=%s\n' Where "$target_dir"
	printf '%s=ro,lowerdir=%s\n' Options "$source_path":"$base_dir"
	printf '\n[Install]\n'
	echo "WantedBy=user@$UID.service"
}

case "$mode" in
	"mount")
		if ! dir_empty "$target_dir"; then
			printf 'Target path \033[1;36m%s\033[0m is not empty!\n' "$target_dir" >&2
			exit 1
		fi
		ensure_dir "$target_dir"
		_sudo mount -t overlay overlay "$target_dir" -o ro,lowerdir="$source_path":"$base_dir"
		;;
	"systemd")
		systemd_unit="$(systemd_escape "$target_dir")".mount
		systemd_target=/etc/systemd/system/"$systemd_unit"
		print_values systemd_target "$systemd_target" >&2
		dump_systemd | _sudo tee "$systemd_target"
		_sudo umount "$target_dir" || true
		_sudo systemctl daemon-reload
		_sudo systemctl restart "$systemd_unit"
		;;
	*)
		printf 'Unknown mode: %s\n' "$mode" >&2
		exit 1
		;;
esac
